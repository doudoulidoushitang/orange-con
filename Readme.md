###客户端
    通过orange.grpc.client配置grpc客户端信息，-> GrpcClientProperties
    配置后会自动通过配置的服务端ip和端口进行连接

    通过orange.netty.client配置netty客户端信息， ->  NettyClientProperties
    配置后自动通过配置的服务端ip和端口进行连接

###服务端
    通过配置orange.grpc.server或者orange.netty.server配置本地服务端的端口
    继承ServerInterceptor 来实现grpc的服务端拦截器

###消息处理
    客户端和服务端的消息处理都统一通过消息处理器来处理，继承RequestHandler并使用@MsgHandler接口来实现一个消息处理器

###自动配置
grpc相关配置为OrangeGrpcServerConfiguration
netty相关配置为OrangeNettyConfigure

###连接管理
对于客户端的连接和断开，会通过发布事件ClientConnectEvent来通知，实现EventHandler接口来监听事件并处理
