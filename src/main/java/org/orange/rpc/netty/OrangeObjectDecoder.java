package org.orange.rpc.netty;

import org.orange.rpc.utils.ReflectUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class OrangeObjectDecoder extends ByteToMessageDecoder{

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		Object obj = decodeMsg(in);
		if(obj != null) {
			out.add(obj);
		}
	}
	
	private Object decodeMsg(ByteBuf buf) {
		buf.markReaderIndex();
		if(buf.readableBytes() <2) {
			return null ; 
		}
		byte startByte = buf.readByte();
		byte chekeByte = buf.readByte() ;
		while(0x68 != startByte || chekeByte != 0x04) {
			if(buf.readableBytes() >1) {
				startByte = buf.readByte();
				chekeByte = buf.readByte() ;
			}else {
				return null;
			}
		}
		try {
			if(buf.readableBytes() <4) {
				buf.resetReaderIndex();
				return null ; 
			}
			byte[] lengthByte = new byte[4] ;
			buf.readBytes(lengthByte);
			int length = ((((lengthByte[0]&0xFF)<< 24)  | ((lengthByte[1]&0xFF) << 16) | ((lengthByte[2]&0xFF) << 8) | (lengthByte[3]&0xFF) ) & 0xFFFFFFFF);
			if(length < 0 || length >= Integer.MAX_VALUE) {
				return null ;
			}
			byte[] data = new byte[length];
			if(buf.readableBytes() < length) {
				buf.resetReaderIndex();
				return null ;
			}
			buf.readBytes(data);
			return ReflectUtils.deSearialMsg(data);
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}
	}
}
