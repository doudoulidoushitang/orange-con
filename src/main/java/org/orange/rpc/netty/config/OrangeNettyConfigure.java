package org.orange.rpc.netty.config;

import org.orange.rpc.netty.NettyMessageHandler;
import org.orange.rpc.netty.client.BaseNettyClient;
import org.orange.rpc.netty.client.OrangeNettyClient;
import org.orange.rpc.netty.server.BaseNettyServer;
import org.orange.rpc.netty.server.OrangeNettyServer;
import org.orange.rpc.top.model.RequestRestLoad;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({NettyServerProperties.class, NettyClientProperties.class})
public class OrangeNettyConfigure {

    @Bean
    @ConditionalOnProperty(prefix = "orange.netty.client",name = "serverIp")
    public BaseNettyClient orangeNettyClient(@Autowired NettyClientProperties clientProperties){
        return new OrangeNettyClient(clientProperties);
    }

    @Bean
    @ConditionalOnProperty(prefix = "orange.netty.server",name = "port")
    public BaseNettyServer orangeNettyServer(@Autowired  NettyServerProperties serverProperties){
        return new OrangeNettyServer(serverProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public NettyMessageHandler messageHandler(){
        return new DefaultMessageHandler();
    }

    class DefaultMessageHandler implements NettyMessageHandler{

        private final Logger log = LoggerFactory.getLogger(DefaultMessageHandler.class);

        @Override
        public void dealMsg(ChannelHandlerContext ctx, RequestRestLoad smg) {
            log.info("you have received a netty msg ,please deal it by implements NettyMessageHandler");
        }

        @Override
        public void connected(ChannelHandlerContext ctx) {
        }

        @Override
        public void disConnected(ChannelHandlerContext ctx) {
        }
    }
}
