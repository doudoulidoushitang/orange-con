package org.orange.rpc.netty.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "orange.netty.server")
public class NettyServerProperties {

    private int port = 2906 ;

    private int maxInboundMessageSize = 10*1024*1024 ;


}
