package org.orange.rpc.netty;

import org.orange.rpc.utils.ReflectUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.Serializable;

public class OrangeObjectEncoder extends MessageToByteEncoder<Serializable>{

	@Override
	protected void encode(ChannelHandlerContext ctx, Serializable msg, ByteBuf out) throws Exception {
		byte[] data = ReflectUtils.serialMsg(msg);
		if(data != null && data.length > 0) {
			out.writeBytes(data);
		}
	}

}
