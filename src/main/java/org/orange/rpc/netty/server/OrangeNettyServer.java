package org.orange.rpc.netty.server;

import org.orange.rpc.netty.config.NettyServerProperties;
import org.orange.rpc.utils.GlobalExecutorUtils;

import java.util.concurrent.ThreadPoolExecutor;

public class OrangeNettyServer extends BaseNettyServer{

    private NettyServerProperties serverProperties ;

    public OrangeNettyServer(NettyServerProperties serverProperties) {
        super();
        this.serverProperties = serverProperties;
    }

    @Override
    protected int getMaxInboundMessageSize() {
        return serverProperties.getMaxInboundMessageSize();
    }

    @Override
    public ThreadPoolExecutor getExecutor() {
        return GlobalExecutorUtils.getNettyServerExecutor() ;
    }

    @Override
    public int getServerPort() {
        return serverProperties.getPort();
    }
}
