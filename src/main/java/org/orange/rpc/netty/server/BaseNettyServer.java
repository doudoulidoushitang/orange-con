package org.orange.rpc.netty.server;

import org.orange.rpc.netty.MessageHandlerAdapter;
import org.orange.rpc.netty.NettyMessageHandler;
import org.orange.rpc.netty.OrangeObjectDecoder;
import org.orange.rpc.netty.OrangeObjectEncoder;
import org.orange.rpc.top.RpcServer;
import org.orange.rpc.utils.EnvUtils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseNettyServer implements RpcServer,SmartLifecycle , InitializingBean {

    protected final static Logger log = LoggerFactory.getLogger(BaseNettyServer.class);

    private EventLoopGroup workGroup = new NioEventLoopGroup(EnvUtils.getDefaultExecutorCors());

    private AtomicBoolean started = new AtomicBoolean(false);

    @Autowired
    private NettyMessageHandler handler;

    private ServerBootstrap bootstrap ;

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("start init nnetty server ... ");
        bootstrap = new ServerBootstrap();
        bootstrap.group(workGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE,true)
                .option(ChannelOption.SO_RCVBUF,getMaxInboundMessageSize())
                .option(ChannelOption.TCP_NODELAY,true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new OrangeObjectDecoder());
                        pipeline.addLast(new OrangeObjectEncoder());
                        pipeline.addLast(new MessageHandlerAdapter(getExecutor(),handler));
                    }
                });
    }

    @Override
    public void start() {
        log.info("netty server start bind port on {}",getServerPort());
        bootstrap.bind(getServerPort());
        started.compareAndSet(false,true);
        log.info("netty server started .");
    }

    @Override
    public void stop() {
        workGroup.shutdownNow();
        started.compareAndSet(true ,false);
        log.info("netty server have stop .");
    }

    protected abstract int getMaxInboundMessageSize();

    @Override
    public boolean isRunning() {
        return started.get();
    }

    public abstract ThreadPoolExecutor getExecutor();

    public abstract int getServerPort();
}
