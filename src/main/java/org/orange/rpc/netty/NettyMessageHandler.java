package org.orange.rpc.netty;

import org.orange.rpc.top.model.RequestRestLoad;
import io.netty.channel.ChannelHandlerContext;

public interface NettyMessageHandler {

    void dealMsg(ChannelHandlerContext ctx , RequestRestLoad smg);

    void connected(ChannelHandlerContext ctx);

    void disConnected(ChannelHandlerContext ctx);
}
