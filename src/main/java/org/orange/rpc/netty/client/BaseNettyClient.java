package org.orange.rpc.netty.client;

import org.orange.rpc.netty.MessageHandlerAdapter;
import org.orange.rpc.netty.NettyMessageHandler;
import org.orange.rpc.netty.OrangeObjectDecoder;
import org.orange.rpc.netty.OrangeObjectEncoder;
import org.orange.rpc.netty.server.BaseNettyServer;
import org.orange.rpc.top.model.ServerNode;
import org.orange.rpc.utils.EnvUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;

import java.net.InetSocketAddress;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseNettyClient implements SmartLifecycle {
    protected final static Logger log = LoggerFactory.getLogger(BaseNettyServer.class);
    private Channel channel;

    private Bootstrap boot = new Bootstrap();
    private AtomicBoolean started = new AtomicBoolean(false);
    private EventLoopGroup workGroup = new NioEventLoopGroup(EnvUtils.getDefaultExecutorCors());

    @Autowired
    private NettyMessageHandler handler;

    @Override
    public void start() {
        log.info("start init netty client boot .");
        try {
            boot.group(workGroup)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.SO_RCVBUF,getMaxInboundMessageSize())
                    .option(ChannelOption.SO_TIMEOUT, getKeepAliveTime())
                    .channel(NioSocketChannel.class)
                    .remoteAddress(new InetSocketAddress(getRemoteServer().getIp(), getRemoteServer().getPort()))
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            pipeline.addLast(new OrangeObjectDecoder());
                            pipeline.addLast(new OrangeObjectEncoder());
                            pipeline.addLast(new MessageHandlerAdapter(getExecutor(),handler));
                        }
                    });
            ChannelFuture future = boot.connect().awaitUninterruptibly();
            started .compareAndSet(false,true);
            if(future.isSuccess()) {
                channel = future.channel() ;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void stop() {
        workGroup.shutdownNow();
        started.compareAndSet(true ,false);
    }

    @Override
    public boolean isRunning() {
        return started.get();
    }

    public abstract ThreadPoolExecutor getExecutor();

    public abstract ServerNode getRemoteServer() ;

    protected void afterStarted(){}

    protected abstract int getReconnectInterval();

    protected abstract int getMaxInboundMessageSize();

    protected int getKeepAliveTime(){
        return 6*60*1000 ;
    }

    public Channel getChannel(){
        return channel ;
    }
}
