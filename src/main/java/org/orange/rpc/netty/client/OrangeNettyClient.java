package org.orange.rpc.netty.client;

import org.orange.rpc.netty.config.NettyClientProperties;
import org.orange.rpc.top.model.RequestRestLoad;
import org.orange.rpc.top.model.ServerNode;
import org.orange.rpc.utils.GlobalExecutorUtils;

import java.util.concurrent.ThreadPoolExecutor;

public class OrangeNettyClient extends BaseNettyClient{

    private NettyClientProperties clientProperties;

    public OrangeNettyClient(NettyClientProperties clientProperties){
        this.clientProperties = clientProperties;
    }

    @Override
    public ThreadPoolExecutor getExecutor() {
        return GlobalExecutorUtils.getNettyClientExecutor();
    }

    @Override
    public ServerNode getRemoteServer() {
        return ServerNode.newServer(clientProperties.getServerIp(),clientProperties.getServerPort());
    }

    @Override
    protected int getReconnectInterval() {
        return clientProperties.getReconnectInterval();
    }

    @Override
    protected int getMaxInboundMessageSize() {
        return clientProperties.getMaxInboundMessageSize();
    }

    public void sendMsg(RequestRestLoad restLoad){
        getChannel().writeAndFlush(restLoad);
    }

}
