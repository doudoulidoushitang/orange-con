package org.orange.rpc.netty;

import org.orange.rpc.top.model.RequestRestLoad;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.ThreadPoolExecutor;

public class MessageHandlerAdapter extends SimpleChannelInboundHandler<RequestRestLoad> {

    private ThreadPoolExecutor executor ;

    private NettyMessageHandler msgHandler ;

    public MessageHandlerAdapter(ThreadPoolExecutor executor, NettyMessageHandler msgHandler){
        this.executor = executor ;
        this.msgHandler = msgHandler;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RequestRestLoad msg) throws Exception {
        executor.execute(()->msgHandler.dealMsg(ctx,msg));
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        executor.execute(()->msgHandler.connected(ctx));
    }

    /**
     * Do nothing by default, sub-classes may override this method.
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        executor.execute(()->msgHandler.disConnected(ctx));
    }

}
