package org.orange.rpc;

import org.orange.rpc.utils.EnvUtils;
import lombok.Getter;

@Getter
public class GrpcServerConfig {

    private int executorCoreSize = EnvUtils.getDefaultExecutorCors();

    private int executorQueueSize = EnvUtils.DEFAULT_EXECUTOR_QUEUE_SIZE ;

    private int maxInboundMessageSize = 10*1024*1024 ;

}
