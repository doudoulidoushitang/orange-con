package org.orange.rpc;

import org.orange.rpc.anno.MessageHandlerInitizer;
import org.orange.rpc.grpc.config.OrangeGrpcServerConfiguration;
import org.orange.rpc.netty.config.OrangeNettyConfigure;
import org.orange.rpc.top.OrangeGlobalConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({OrangeGlobalConfiguration.class,OrangeGrpcServerConfiguration.class,
        MessageHandlerInitizer.class, OrangeNettyConfigure.class})
public class OrangeConnectionConfiguration {

}
