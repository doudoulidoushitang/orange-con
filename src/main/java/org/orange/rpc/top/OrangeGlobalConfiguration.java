package org.orange.rpc.top;

import org.orange.common.base.event.AbstractEventLoop;
import org.orange.common.base.event.AsyncEventLoop;
import org.orange.rpc.grpc.config.OrangeGrpcServerConfiguration;
import org.orange.rpc.handler.ApplicationContextHolder;
import org.orange.rpc.netty.config.OrangeNettyConfigure;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@AutoConfigureBefore({OrangeGrpcServerConfiguration.class, OrangeNettyConfigure.class})
public class OrangeGlobalConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public AsyncEventLoop eventLoop(){
        return new AbstractEventLoop() {
            @Override
            protected ThreadPoolExecutor getExecutor() {
                return null;
            }
        };
    }

    @Bean
    public ApplicationContextHolder contextHolder(){
        return new ApplicationContextHolder();
    }
}
