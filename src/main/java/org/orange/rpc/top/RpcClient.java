package org.orange.rpc.top;

import org.orange.rpc.top.model.RequestRestLoad;

import java.io.Closeable;
import java.util.concurrent.Future;

public interface RpcClient extends Closeable {

    void init();

    Future<RequestRestLoad> request(RequestRestLoad request);

    void requestIgnoreResponse(RequestRestLoad request);
}
