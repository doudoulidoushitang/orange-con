package org.orange.rpc.top.event;

import org.orange.common.base.event.AsyncEvent;
import org.orange.rpc.top.constants.ConnectionStatus;
import lombok.Getter;

@Getter
public class ClientConnectEvent implements AsyncEvent {

    private String clientMark ;

    private ConnectionStatus status ;

    public ClientConnectEvent(String clientMark ,ConnectionStatus status){
        this.clientMark = clientMark ;
        this.status = status ;
    }

    public static ClientConnectEvent newInstance(String clientMark ,ConnectionStatus status){
        return new ClientConnectEvent(clientMark ,status);
    }
}
