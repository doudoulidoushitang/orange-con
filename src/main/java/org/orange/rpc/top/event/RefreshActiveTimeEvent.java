package org.orange.rpc.top.event;

import org.orange.common.base.event.AsyncEvent;
import lombok.Getter;

@Getter
public class RefreshActiveTimeEvent implements AsyncEvent {

    private String mark ;

    public RefreshActiveTimeEvent(String mark){
        this.mark = mark ;
    }
}
