package org.orange.rpc.top.model;

import com.google.common.util.concurrent.ListenableFuture;
import org.orange.rpc.grpc.auto.RestLoad;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class GrpcRequestFuture implements Future<RequestRestLoad> {

    private ListenableFuture<RestLoad.Restload> grpcFuture ;

    public GrpcRequestFuture(ListenableFuture<RestLoad.Restload> grpcFuture){
        this.grpcFuture = grpcFuture ;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return grpcFuture.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return grpcFuture.isCancelled();
    }

    @Override
    public boolean isDone() {
        return grpcFuture.isDone();
    }

    @Override
    public RequestRestLoad get() throws InterruptedException, ExecutionException {
        RestLoad.Restload restload = grpcFuture.get();
        return RequestRestLoad.formatRestLoad(restload);
    }

    @Override
    public RequestRestLoad get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        RestLoad.Restload restload = grpcFuture.get(timeout,unit);
        return RequestRestLoad.formatRestLoad(restload);
    }
}
