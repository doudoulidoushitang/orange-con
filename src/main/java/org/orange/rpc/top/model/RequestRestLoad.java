package org.orange.rpc.top.model;

import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import org.orange.rpc.grpc.auto.MetaData;
import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.utils.ReflectUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
public class RequestRestLoad implements Serializable {
    private static AtomicInteger sequeueNo = new AtomicInteger(0);
    private Map<String,String> headers = new HashMap<>();

    private String type ;

    private String sequeue ;

    private String client = "";

    private Object body ;

    public static RequestRestLoad newInstance(){
        return new RequestRestLoad();
    }

    public static RequestRestLoad newInstance(Object body) {
        return new RequestRestLoad().body(body) ;
    }

    public RequestRestLoad headers(Map<String,String> headers){
        this.headers = headers;
        return this ;
    }

    public RequestRestLoad addHeader(String key , String value){
        if(Objects.isNull(this.headers)){
            this.headers = new HashMap<>();
        }
        this.headers.put(key,value);
        return this ;
    }

    public RequestRestLoad client(String clientInfo){
        this.client = clientInfo ;
        return this ;
    }

    public RequestRestLoad body(Object body){
        this.body = body ;
        return this ;
    }

    public RestLoad.Restload toRestLoad(){
        MetaData.Metadata metadata = MetaData.Metadata.newBuilder().setType(getBody().getClass().getTypeName())
                .setClient(getClient()).setSequeue("Seq-"+sequeueNo.incrementAndGet()).putAllHeaders(getHeaders()).build();

        String bodyString = ReflectUtils.toJson(getBody());
        RestLoad.Restload result =RestLoad.Restload.newBuilder()
                .setBody(Any.newBuilder()
                        .setValue(ByteString.copyFrom(bodyString, Charset.forName("UTF-8"))))
                .setMetadata(metadata)
                .build();
        return result ;
    }

    public static RequestRestLoad formatRestLoad(RestLoad.Restload restload) {
        if(Objects.isNull(restload)){
            return null ;
        }
        RequestRestLoad res = newInstance();
        String className =restload.getMetadata().getType();
        String body = restload.getBody().getValue().toString(Charset.forName("UTF-8")) ;
        try{
            res.body(ReflectUtils.toObject(body ,Class.forName(className)))
                    .client(restload.getMetadata().getClient())
                    .headers(restload.getMetadata().getHeadersMap());
        }catch (Exception e){
            e.printStackTrace();
        }
        res.setSequeue(restload.getMetadata().getSequeue());
        res.setType(className);
        return res ;
    }
}
