package org.orange.rpc.top.model;

import org.orange.rpc.exception.OrangeRpcException;
import org.orange.rpc.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class ServerNode {

    private String ip ;

    private Integer port ;

    public ServerNode(String ip , Integer port){
        if(StringUtils.isBlank(ip)){
            throw new OrangeRpcException("ip is not valid .");
        }
        this.ip = ip ;
        if(Objects.isNull(port) || port < 0){
            throw new OrangeRpcException("port is not valid .");
        }
        this.port = port ;
    }

    public static ServerNode newServer(String ip , Integer port){
        return new ServerNode(ip , port);
    }

    public String getMark(){
        return ip+":"+port ;
     }
}
