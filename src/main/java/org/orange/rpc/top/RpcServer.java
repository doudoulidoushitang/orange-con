package org.orange.rpc.top;

public interface RpcServer {

    void start();

    void stop();
}
