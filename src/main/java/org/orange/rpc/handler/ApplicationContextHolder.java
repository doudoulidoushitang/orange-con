package org.orange.rpc.handler;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

public class ApplicationContextHolder implements ApplicationContextAware {

    private static ApplicationContext applicationContext ;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = applicationContext ;
    }

    public static <T> T getBean(Class<T> type){
        return applicationContext.getBean(type);
    }

    public static <T> Map<String, T> getBeansByType(Class<T> type){
        return applicationContext.getBeansOfType(type);
    }
}
