package org.orange.rpc.handler;

import org.orange.rpc.message.MessageHandler;
import org.orange.rpc.message.RpcMessage;
import org.orange.rpc.top.model.ConnType;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class RegistHandlerManager{

    private static Map<ConnType,ConcurrentHashMap<String,MessageHandler>> handlerMap = new ConcurrentHashMap<ConnType, ConcurrentHashMap<String, MessageHandler>>();

    public static void putHandlerBean(ConnType connType, Class<? extends RpcMessage> type , MessageHandler bean) {
        if(Objects.nonNull(type)&&Objects.nonNull(connType)&&Objects.nonNull(bean)) {
            handlerMap.computeIfAbsent(connType,
                        (k)->{return new ConcurrentHashMap<>();}
                    ).put(type.getTypeName(),bean);
        }
    }

    public static MessageHandler getHandlerBean(ConnType connType, String type) {
        if(Objects.isNull(type)|| !handlerMap.containsKey(connType)) {
            return null ;
        }
        return handlerMap.get(connType).get(type);
    }

    public static MessageHandler getHandlerBean(ConnType connType , Class<? extends RpcMessage> type) {
        if(Objects.isNull(type)|| !handlerMap.containsKey(connType)) {
            return null ;
        }
        return handlerMap.get(connType).get(type.getTypeName());
    }
}
