package org.orange.rpc.exception;

public class OrangeRpcException extends RuntimeException{

    public OrangeRpcException(){
        super();
    }

    public OrangeRpcException(String msg){
        super(msg);
    }

    public OrangeRpcException(Exception e){
        super(e);
    }
}

