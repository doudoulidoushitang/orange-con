package org.orange.rpc.anno;

import org.orange.rpc.handler.RegistHandlerManager;
import org.orange.rpc.message.MessageHandler;
import org.orange.rpc.message.RpcMessage;
import org.orange.rpc.top.model.ConnType;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.Objects;

public class MessageHandlerInitizer implements BeanPostProcessor {

    @SuppressWarnings("unchecked")
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        MsgHandler handler = bean.getClass().getAnnotation(MsgHandler.class) ;
        if(Objects.nonNull(handler) && bean instanceof MessageHandler) {
            Class<? extends RpcMessage>[] types = handler.value();
            ConnType[] conns = handler.type() ;
            for(ConnType connType:conns){
                for(Class<? extends RpcMessage> type:types) {
                    RegistHandlerManager.putHandlerBean(connType ,type, (MessageHandler) bean);
                }
            }
        }
        return bean;
    }
}
