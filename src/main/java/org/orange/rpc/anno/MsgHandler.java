package org.orange.rpc.anno;


import org.orange.rpc.message.RpcMessage;
import org.orange.rpc.top.model.ConnType;
import org.springframework.stereotype.Service;
import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
public @interface MsgHandler {
    Class<? extends RpcMessage>[] value();

    ConnType[] type() ;
}
