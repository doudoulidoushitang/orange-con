package org.orange.rpc.grpc.server.supplier;

import org.orange.rpc.utils.ReflectUtils;
import io.grpc.*;
import io.grpc.internal.ServerStream;
import org.orange.rpc.grpc.constants.ServerContextKeys;

import java.util.function.Supplier;

public class GrpcServerInterceptorSupplier implements Supplier<ServerInterceptor> {

    @Override
    public ServerInterceptor get() {
        return new ServerInterceptor() {
            @Override
            public <T, S> ServerCall.Listener<T> interceptCall(ServerCall<T, S> call, Metadata headers, ServerCallHandler<T, S> next) {
                Context ctx = Context.current()
                        .withValue(ServerContextKeys.CONTEXT_KEY_CONN_ID, call.getAttributes().get(ServerContextKeys.TRANS_KEY_CONN_ID))
                        .withValue(ServerContextKeys.CONTEXT_KEY_CONN_REMOTE_IP, call.getAttributes().get(ServerContextKeys.TRANS_KEY_REMOTE_IP))
                        .withValue(ServerContextKeys.CONTEXT_KEY_CONN_REMOTE_PORT, call.getAttributes().get(ServerContextKeys.TRANS_KEY_REMOTE_PORT))
                        .withValue(ServerContextKeys.CONTEXT_KEY_CONN_LOCAL_PORT, call.getAttributes().get(ServerContextKeys.TRANS_KEY_LOCAL_PORT));
                return Contexts.interceptCall(ctx, call, headers, next);
            }
        };
    }

    private Channel getInternalChannel(ServerCall serverCall) {
        ServerStream serverStream = (ServerStream) ReflectUtils.getFieldValue(serverCall, "stream");
        return (Channel)ReflectUtils.getFieldValue(serverStream, "channel");
    }
}
