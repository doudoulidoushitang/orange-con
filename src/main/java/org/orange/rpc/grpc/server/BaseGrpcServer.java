package org.orange.rpc.grpc.server;

import org.orange.rpc.grpc.server.supplier.BiStreamServiceDefinitionSupplier;
import org.orange.rpc.grpc.server.supplier.RequestServiceDefinitionSupplier;
import org.orange.rpc.top.RpcServer;
import io.grpc.*;
import io.grpc.util.MutableHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;


public abstract class BaseGrpcServer implements RpcServer,SmartLifecycle , InitializingBean {

    protected final static Logger log = LoggerFactory.getLogger(BaseGrpcServer.class);

    private Server server ;

    private AtomicBoolean started = new AtomicBoolean(false);

    @Autowired(required = false)
    private List<ServerInterceptor> serverInterceptors = Collections.emptyList() ;

    @Autowired
    private ServerTransportFilter transportFilter;

    @Autowired
    private BiStreamServiceDefinitionSupplier biStreamMethodDescriptorSupplier;

    @Autowired
    private RequestServiceDefinitionSupplier requestMethodDescriptorSupplier;

    public void start() {
        try{
            server.start();
            started.compareAndSet(false ,true);
            log.info("start orange grpc server complete. and listen on port {}" ,getServerPort());
        }catch (IOException e){
            stop();
            log.error("start orange grpc server failed .",e);
        }
    }

    @Override
    public void stop() {
        log.warn("stop orange grpc server .");
        server.shutdown();
        started.compareAndSet(true,false);
    }

    @Override
    public boolean isRunning() {
        return started.get();
    }

    private void addServices(MutableHandlerRegistry handlerRegistry) {
        log.info("start add grpc services .");
        handlerRegistry.addService(ServerInterceptors.intercept(requestMethodDescriptorSupplier.get(), serverInterceptors));

        handlerRegistry.addService(ServerInterceptors.intercept(biStreamMethodDescriptorSupplier.get(), serverInterceptors));
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        final MutableHandlerRegistry handlerRegistry = new MutableHandlerRegistry();

        addServices(handlerRegistry);

        server = ServerBuilder.forPort(getServerPort())
                .executor(getExecutor())
                .maxInboundMessageSize(getMaxInboundMessageSize())
                .fallbackHandlerRegistry(handlerRegistry)
                .compressorRegistry(CompressorRegistry.getDefaultInstance())
                .decompressorRegistry(DecompressorRegistry.getDefaultInstance())
                .addTransportFilter(transportFilter)
                .build();

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run(){
                BaseGrpcServer.this.stop();
            }
        });
    }

    public abstract ThreadPoolExecutor getExecutor();

    public abstract int getServerPort();

    protected int getMaxInboundMessageSize(){
        return 10*1024*1024 ;
    }
}
