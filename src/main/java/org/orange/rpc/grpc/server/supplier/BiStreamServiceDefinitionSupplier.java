package org.orange.rpc.grpc.server.supplier;

import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.grpc.handler.BiStreamHandler;
import org.orange.rpc.utils.EnvUtils;
import io.grpc.MethodDescriptor;
import io.grpc.ServerCallHandler;
import io.grpc.ServerServiceDefinition;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.ServerCalls;

import java.util.function.Supplier;

public class BiStreamServiceDefinitionSupplier implements Supplier<ServerServiceDefinition> {

    private BiStreamHandler streamHandler ;

    public BiStreamServiceDefinitionSupplier(BiStreamHandler streamHandler){
        this.streamHandler = streamHandler;
    }

    @Override
    public ServerServiceDefinition get() {
        // bi stream register.
        ServerCallHandler<RestLoad.Restload, RestLoad.Restload> biStreamHandler = ServerCalls.asyncBidiStreamingCall(
                (responseObserver) -> streamHandler.handle(responseObserver));

        MethodDescriptor<RestLoad.Restload, RestLoad.Restload> biStreamMethod = MethodDescriptor.<RestLoad.Restload, RestLoad.Restload>newBuilder()
                .setType(MethodDescriptor.MethodType.BIDI_STREAMING).setFullMethodName(MethodDescriptor
                        .generateFullMethodName(EnvUtils.REQUEST_BI_STREAM_SERVICE_NAME, EnvUtils.REQUEST_BI_STREAM_METHOD_NAME))
                .setRequestMarshaller(ProtoUtils.marshaller(RestLoad.Restload.newBuilder().build()))
                .setResponseMarshaller(ProtoUtils.marshaller(RestLoad.Restload.getDefaultInstance())).build();

        ServerServiceDefinition serviceDefOfBiStream = ServerServiceDefinition
                .builder(EnvUtils.REQUEST_BI_STREAM_SERVICE_NAME).addMethod(biStreamMethod, biStreamHandler).build();

        return serviceDefOfBiStream;
    }

}
