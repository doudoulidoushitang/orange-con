package org.orange.rpc.grpc.server;

import org.orange.rpc.grpc.config.GrpcServerProperties;
import org.orange.rpc.utils.GlobalExecutorUtils;

import java.util.concurrent.ThreadPoolExecutor;

public class OrangeGrpcServer extends BaseGrpcServer{

    private GrpcServerProperties serverProperties ;

    public OrangeGrpcServer(GrpcServerProperties serverProperties) {
        super();
        this.serverProperties = serverProperties;
    }

    @Override
    public ThreadPoolExecutor getExecutor() {
        return GlobalExecutorUtils.getGrpcServerExecutor();
    }

    @Override
    public int getServerPort() {
        return serverProperties.getPort();
    }
}
