package org.orange.rpc.grpc.server.filter;

import org.orange.common.base.event.AsyncEventLoop;
import org.orange.rpc.grpc.constants.ServerContextKeys;
import org.orange.rpc.top.constants.ConnectionStatus;
import org.orange.rpc.top.event.ClientConnectEvent;
import org.orange.rpc.utils.StringUtils;
import io.grpc.Attributes;
import io.grpc.Grpc;
import io.grpc.ServerTransportFilter;

import java.net.InetSocketAddress;

public class GrpcServerTransportFilter extends ServerTransportFilter {

    public AsyncEventLoop eventLoop ;

    public GrpcServerTransportFilter(AsyncEventLoop eventLoop){
        this.eventLoop = eventLoop ;
    }

    @Override
    public Attributes transportReady(Attributes transportAttrs) {
        InetSocketAddress remoteAddress = (InetSocketAddress) transportAttrs
                .get(Grpc.TRANSPORT_ATTR_REMOTE_ADDR);
        InetSocketAddress localAddress = (InetSocketAddress) transportAttrs
                .get(Grpc.TRANSPORT_ATTR_LOCAL_ADDR);
        int remotePort = remoteAddress.getPort();
        int localPort = localAddress.getPort();
        String remoteIp = remoteAddress.getAddress().getHostAddress();
        Attributes attrWrapper = transportAttrs.toBuilder()
                .set(ServerContextKeys.TRANS_KEY_REMOTE_IP, remoteIp)
                .set(ServerContextKeys.TRANS_KEY_REMOTE_PORT, remotePort)
                .set(ServerContextKeys.TRANS_KEY_LOCAL_PORT, localPort).build();
        eventLoop.publishEvent(
                ClientConnectEvent.newInstance(
                        StringUtils.formatServerMark(remoteIp,remotePort), ConnectionStatus.CONNECTED));
        return attrWrapper;
    }

    @Override
    public void transportTerminated(Attributes transportAttrs) {
        String remoteIp = transportAttrs.get(ServerContextKeys.TRANS_KEY_REMOTE_IP);
        Integer remotePort = transportAttrs.get(ServerContextKeys.TRANS_KEY_REMOTE_PORT);
        eventLoop.publishEvent(
                ClientConnectEvent.newInstance(
                        StringUtils.formatServerMark(remoteIp,remotePort), ConnectionStatus.DISCONNECTED));
    }
}
