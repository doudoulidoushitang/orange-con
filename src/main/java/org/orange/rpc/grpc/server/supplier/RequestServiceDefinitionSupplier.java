package org.orange.rpc.grpc.server.supplier;

import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.grpc.handler.RequestHandler;
import org.orange.rpc.utils.EnvUtils;
import io.grpc.MethodDescriptor;
import io.grpc.ServerCallHandler;
import io.grpc.ServerServiceDefinition;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.ServerCalls;

import java.util.function.Supplier;

public class RequestServiceDefinitionSupplier implements Supplier<ServerServiceDefinition> {
    private RequestHandler requestHandler ;

    public RequestServiceDefinitionSupplier(RequestHandler requestHandler){
        this.requestHandler = requestHandler;
    }

    @Override
    public ServerServiceDefinition get() {
        // unary common call register.
        MethodDescriptor<RestLoad.Restload, RestLoad.Restload> unaryPayloadMethod = MethodDescriptor.<RestLoad.Restload, RestLoad.Restload>newBuilder()
                .setType(MethodDescriptor.MethodType.UNARY)
                .setFullMethodName(MethodDescriptor.generateFullMethodName(EnvUtils.REQUEST_SERVICE_NAME, EnvUtils.REQUEST_METHOD_NAME))
                .setRequestMarshaller(ProtoUtils.marshaller(RestLoad.Restload.getDefaultInstance()))
                .setResponseMarshaller(ProtoUtils.marshaller(RestLoad.Restload.getDefaultInstance())).build();

        ServerCallHandler<RestLoad.Restload, RestLoad.Restload> payloadHandler = ServerCalls
                .asyncUnaryCall((request, responseObserver) -> {
                    requestHandler.handle(request,responseObserver);
                });

        ServerServiceDefinition serviceDefOfUnaryPayload = ServerServiceDefinition.builder(EnvUtils.REQUEST_SERVICE_NAME)
                .addMethod(unaryPayloadMethod, payloadHandler).build();

        return serviceDefOfUnaryPayload;
    }
}
