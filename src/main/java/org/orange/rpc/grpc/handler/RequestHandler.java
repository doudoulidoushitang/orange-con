package org.orange.rpc.grpc.handler;

import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.message.MessageHandler;
import io.grpc.stub.StreamObserver;

public interface RequestHandler extends MessageHandler {

    void handle(RestLoad.Restload grpcRequest, StreamObserver<RestLoad.Restload> responseObserver);
}
