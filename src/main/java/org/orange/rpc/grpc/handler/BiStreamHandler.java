package org.orange.rpc.grpc.handler;

import org.orange.rpc.grpc.auto.RestLoad;
import io.grpc.stub.StreamObserver;

public interface BiStreamHandler {

    StreamObserver<RestLoad.Restload> handle(StreamObserver<RestLoad.Restload> responseObserver);
}
