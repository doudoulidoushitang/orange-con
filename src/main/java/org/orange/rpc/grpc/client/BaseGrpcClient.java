package org.orange.rpc.grpc.client;

import io.grpc.stub.StreamObserver;
import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.handler.ApplicationContextHolder;
import org.orange.rpc.top.RpcClient;
import org.orange.rpc.top.model.RequestRestLoad;
import org.orange.rpc.top.model.ServerNode;
import io.grpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseGrpcClient implements RpcClient {

    protected final static Logger log = LoggerFactory.getLogger(BaseGrpcClient.class);

    private Channel channel ;

    private ManagedChannel managedChannel ;

    protected AtomicBoolean inited = new AtomicBoolean(false);

    private GrpcClientRequestManager requestManager ;

    private List<ClientInterceptor> clientInterceptors = Collections.emptyList();

    @Override
    public void init() {
        initInterceptors();
        ServerNode serverNode = getRemoteServer();
        try{
            initChannel(serverNode.getIp() ,serverNode.getPort());
            requestManager = new GrpcClientRequestManager(getChannel());
        }catch (Exception e){
            log.warn("cannot connect to grpc server .will try again .");
        }
        afterInited();
        initizationDone();
        log.info("start orange grpc client complete .");
    }

    private void initInterceptors(){
        Map<String,ClientInterceptor> interceptorMap =ApplicationContextHolder.getBeansByType(ClientInterceptor.class);
        if(Objects.nonNull(interceptorMap)){
            clientInterceptors.addAll(interceptorMap.values());
        }
    }

    @Override
    public void close() {
        managedChannel.shutdown();
        inited.compareAndSet(true,false);
    }

    private void initChannel(String serverIp , int serverPort){
        log.info("start connect to remote grpc server . {}:{}",serverIp , serverPort);
        channel = managedChannel = createChannel(serverIp,serverPort);
        channel = ClientInterceptors.intercept(managedChannel , clientInterceptors);
    }

    protected ManagedChannel createChannel(String serverIp , int serverPort){
        ManagedChannelBuilder channelBuilder = ManagedChannelBuilder.forAddress(serverIp,serverPort)
            .executor(getExecutor())
            .compressorRegistry(CompressorRegistry.getDefaultInstance())
            .decompressorRegistry(DecompressorRegistry.getDefaultInstance())
            .maxInboundMessageSize(getMaxInboundMessageSize())
            .keepAliveTime(getKeepAliveTime(), TimeUnit.MILLISECONDS)
            .usePlaintext();
        return channelBuilder.build() ;
    }

    public abstract ThreadPoolExecutor getExecutor();

    public abstract ServerNode getRemoteServer() ;

    public abstract void requestResponseStream(RequestRestLoad request , StreamObserver<RestLoad.Restload> streamObserver);

    protected void afterInited(){}

    protected int getKeepAliveTime(){
        return 6*60*1000 ;
    }

    protected abstract int getReconnectInterval();

    protected int getMaxInboundMessageSize(){
        return 10*1024*1024 ;
    }

    public Channel getChannel(){
        return channel ;
    }

    public GrpcClientRequestManager getRequestManager(){
        if(!inited.get()){
            init();
        }
        return this.requestManager ;
    }

    protected boolean initizationDone(){
        return inited.compareAndSet(false, true);
    }
}
