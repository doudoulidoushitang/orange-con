package org.orange.rpc.grpc.client;

import io.grpc.MethodDescriptor;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import org.orange.rpc.grpc.auto.BiRequestStreamGrpc;
import org.orange.rpc.grpc.auto.RequestGrpc;
import io.grpc.Channel;
import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.utils.EnvUtils;

import java.util.Objects;

import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;

public class GrpcClientRequestManager {

    private Channel channel ;

    private RequestGrpc.RequestBlockingStub requestBlockingStub ;
    private RequestGrpc.RequestFutureStub requestFutureStub ;

    public GrpcClientRequestManager(Channel channel){
        this.channel = channel ;
    }

    public RequestGrpc.RequestBlockingStub getRequestBlockStub(){
        return Objects.isNull(requestBlockingStub)?(requestBlockingStub = RequestGrpc.newBlockingStub(channel)):requestBlockingStub;
    }

    public RequestGrpc.RequestFutureStub getRequestFutureStub(){
        return Objects.isNull(requestFutureStub)?(requestFutureStub = RequestGrpc.newFutureStub(channel)):requestFutureStub;
    }

    public StreamObserver<RestLoad.Restload> getBiStreamObserver(StreamObserver<RestLoad.Restload> streamObserver){
        return BiRequestStreamGrpc.newStub(channel).requestBiStream(streamObserver);
    }

}
