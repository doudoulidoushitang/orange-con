package org.orange.rpc.grpc.client;

import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.stub.StreamObserver;
import org.orange.rpc.grpc.auto.BiRequestStreamGrpc;
import org.orange.rpc.top.model.GrpcRequestFuture;
import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.grpc.config.GrpcClientProperties;
import org.orange.rpc.top.model.RequestRestLoad;
import org.orange.rpc.top.model.ServerNode;
import org.orange.rpc.utils.GlobalExecutorUtils;

import java.util.concurrent.*;

public class OrangeGrpcClient extends BaseGrpcClient{

    private GrpcClientProperties clientProperties ;

    public OrangeGrpcClient(GrpcClientProperties clientProperties){
        this.clientProperties = clientProperties;
    }

    @Override
    public ThreadPoolExecutor getExecutor() {
        return GlobalExecutorUtils.getGrpcClientxecutor() ;
    }

    @Override
    public ServerNode getRemoteServer() {
        return ServerNode.newServer(clientProperties.getServerIp(),clientProperties.getServerPort());
    }

    @Override
    public void requestResponseStream(RequestRestLoad request, StreamObserver<RestLoad.Restload> streamObserver) {
        StreamObserver<RestLoad.Restload> requestObserver = BiRequestStreamGrpc.newStub(createChannel(getRemoteServer().getIp(),getRemoteServer().getPort())).requestBiStream(streamObserver);
//        StreamObserver<RestLoad.Restload> requestObserver = getRequestManager().getBiStreamObserver(streamObserver);
        requestObserver.onNext(request.toRestLoad());
        requestObserver.onCompleted();
    }

    @Override
    protected int getReconnectInterval() {
        return clientProperties.getReconnectInterval();
    }

    @Override
    public Future<RequestRestLoad> request(RequestRestLoad request){
        ListenableFuture<RestLoad.Restload> response = getRequestManager().getRequestFutureStub().request(request.toRestLoad());
        return new GrpcRequestFuture(response);
    }

    @Override
    public void requestIgnoreResponse(RequestRestLoad request) {
        throw new UnsupportedOperationException("unsupported .");
    }

}
