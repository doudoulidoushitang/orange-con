package org.orange.rpc.grpc.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "orange.grpc.client")
public class GrpcClientProperties {

    private String serverIp ;

    private int serverPort ;

    private int reconnectInterval = 30*1000;

    private int maxInboundMessageSize = 10*1024*1024 ;

}
