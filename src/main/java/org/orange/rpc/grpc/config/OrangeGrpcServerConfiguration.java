package org.orange.rpc.grpc.config;

import io.grpc.ServerInterceptor;
import io.grpc.ServerTransportFilter;
import io.grpc.stub.StreamObserver;
import org.orange.common.base.event.AsyncEventLoop;
import org.orange.rpc.grpc.auto.RestLoad;
import org.orange.rpc.grpc.constants.ServerContextKeys;
import org.orange.rpc.grpc.handler.BiStreamHandler;
import org.orange.rpc.grpc.handler.RequestHandler;
import org.orange.rpc.grpc.server.filter.GrpcServerTransportFilter;
import org.orange.rpc.grpc.server.supplier.BiStreamServiceDefinitionSupplier;
import org.orange.rpc.grpc.server.supplier.GrpcServerInterceptorSupplier;
import org.orange.rpc.grpc.server.supplier.RequestServiceDefinitionSupplier;
import org.orange.rpc.handler.RegistHandlerManager;
import org.orange.rpc.message.MessageHandler;
import org.orange.rpc.top.RpcServer;
import org.orange.rpc.top.constants.ConnectionStatus;
import org.orange.rpc.top.event.ClientConnectEvent;
import org.orange.rpc.top.event.RefreshActiveTimeEvent;
import org.orange.rpc.top.model.ConnType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Objects;

@Configuration
@AutoConfigureAfter(GrpcAutoConfigure.class)
@Import(GrpcAutoConfigure.class)
public class OrangeGrpcServerConfiguration {

    @Bean
    @ConditionalOnBean(RpcServer.class)
    public ServerTransportFilter serverTransportFilter(AsyncEventLoop eventLoop){
        return new GrpcServerTransportFilter(eventLoop);
    }

    @Bean
    @ConditionalOnBean(RpcServer.class)
    public ServerInterceptor serverInterceptor(){
        return new GrpcServerInterceptorSupplier().get();
    }

    @Bean
    @ConditionalOnBean(RpcServer.class)
    public BiStreamServiceDefinitionSupplier biStreamServiceDefinitionSupplier(@Autowired BiStreamHandler biStreamHandler){
        return new BiStreamServiceDefinitionSupplier(biStreamHandler);
    }

    @Bean
    @ConditionalOnBean(RpcServer.class)
    public RequestServiceDefinitionSupplier requestServiceDefinitionSupplier(@Autowired RequestHandler requestHandler){
        return new RequestServiceDefinitionSupplier(requestHandler);
    }

    @Bean
    @ConditionalOnMissingBean
    public RequestHandler requestHandler(AsyncEventLoop eventLoop){
        return new DefaultRequestHandler(eventLoop);
    }

    @Bean
    @ConditionalOnMissingBean
    public BiStreamHandler biStreamHandler(AsyncEventLoop eventLoop){
        return new DefaultRequestHandler(eventLoop);
    }

    class DefaultRequestHandler implements RequestHandler, BiStreamHandler {
        private final Logger log = LoggerFactory.getLogger(DefaultRequestHandler.class);

        private AsyncEventLoop eventLoop;

        public DefaultRequestHandler(AsyncEventLoop eventLoop){
            this.eventLoop = eventLoop;
        }

        @Override
        public void handle(RestLoad.Restload grpcRequest, StreamObserver<RestLoad.Restload> responseObserver) {
            log.debug("grpc server received a message ...");
            String type = grpcRequest.getMetadata().getType() ;
            MessageHandler handler = RegistHandlerManager.getHandlerBean(ConnType.GRPC,type);
            if(Objects.nonNull(handler)){
                ((RequestHandler)handler).handle(grpcRequest,responseObserver);
            }
        }

        @Override
        public StreamObserver<RestLoad.Restload> handle(StreamObserver<RestLoad.Restload> responseObserver) {
            log.debug("grpc server received a bistream message ...");
            StreamObserver<RestLoad.Restload> streamObserver = new StreamObserver<RestLoad.Restload>() {
                final String connectionId = ServerContextKeys.CONTEXT_KEY_CONN_ID.get();

                final Integer localPort = ServerContextKeys.CONTEXT_KEY_CONN_LOCAL_PORT.get();

                final int remotePort = ServerContextKeys.CONTEXT_KEY_CONN_REMOTE_PORT.get();

                String remoteIp = ServerContextKeys.CONTEXT_KEY_CONN_REMOTE_IP.get();
                @Override
                public void onNext(RestLoad.Restload restload) {
                    eventLoop.publishEvent(new RefreshActiveTimeEvent(connectionId));
                    String type = restload.getMetadata().getType() ;
                    MessageHandler handler = RegistHandlerManager.getHandlerBean(ConnType.GRPC,type);
                    if(Objects.nonNull(handler)){
                        ((RequestHandler)handler).handle(restload,responseObserver);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    log.error("error happend ",t);
                    eventLoop.publishEvent(ClientConnectEvent.newInstance(connectionId, ConnectionStatus.DISCONNECTED));
                }

                @Override
                public void onCompleted() {
                    log.info("server stream complete .");
                    eventLoop.publishEvent(ClientConnectEvent.newInstance(connectionId, ConnectionStatus.DISCONNECTED));
                }
            };
            return streamObserver;
        }
    }
}
