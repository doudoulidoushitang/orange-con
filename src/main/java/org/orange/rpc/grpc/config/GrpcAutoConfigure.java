package org.orange.rpc.grpc.config;

import org.orange.rpc.grpc.client.OrangeGrpcClient;
import org.orange.rpc.grpc.server.OrangeGrpcServer;
import org.orange.rpc.top.RpcClient;
import org.orange.rpc.top.RpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({GrpcServerProperties.class,GrpcClientProperties.class})
public class GrpcAutoConfigure {

    @Bean
    @ConditionalOnProperty(prefix = "orange.grpc.client",name = "serverIp")
    public RpcClient orangeGrpcClient(@Autowired GrpcClientProperties clientProperties){
        return new OrangeGrpcClient(clientProperties);
    }

    @Bean
    @ConditionalOnProperty(prefix = "orange.grpc.server",name = "port")
    public RpcServer orangeGrpcServer(@Autowired  GrpcServerProperties serverProperties){
        return new OrangeGrpcServer(serverProperties);
    }
}
