package org.orange.rpc.utils;

import java.util.Objects;

public class StringUtils {

    public static boolean isBlank(String str){
        if(Objects.isNull(str)||"".equals(str)){
            return true ;
        }
        return false ;
    }

    public static boolean isNotBlank(String str){
        return !isBlank(str);
    }

    public static String formatServerMark(String ip ,int port){
        return ip+":"+port ;
    }
}
