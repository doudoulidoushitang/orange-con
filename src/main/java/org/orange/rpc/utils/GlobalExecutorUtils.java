package org.orange.rpc.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.orange.rpc.top.model.NameThreadFactory;

import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class GlobalExecutorUtils {

    private static ThreadPoolExecutor GRPC_CLIENT_EXECUTOR ;

    private static ThreadPoolExecutor GRPC_SERVER_EXECUTOR ;

    private static ScheduledThreadPoolExecutor GRPC_SCHEDULE_EXECUTOR ;

    private static ThreadPoolExecutor NETTY_CLIENT_EXECUTOR ;

    private static ThreadPoolExecutor NETTY_SERVER_EXECUTOR ;

    public static ThreadPoolExecutor getGrpcClientxecutor(){
        if(Objects.isNull(GRPC_CLIENT_EXECUTOR)){
            GRPC_CLIENT_EXECUTOR = new ThreadPoolExecutor(
                    EnvUtils.getAvaliableProcessor(),
                    EnvUtils.getAvaliableProcessor(), 60L, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(100),
                    new ThreadFactoryBuilder().setDaemon(true).setNameFormat("orange-grpc-executor-%d").build());
        }
        return GRPC_CLIENT_EXECUTOR ;
    }

    public static ThreadPoolExecutor getGrpcServerExecutor(){
        if(Objects.isNull(GRPC_SERVER_EXECUTOR)){
            GRPC_SERVER_EXECUTOR = new ThreadPoolExecutor(
                    EnvUtils.getAvaliableProcessor(),
                    EnvUtils.getAvaliableProcessor(), 60L, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(100),
                    new ThreadFactoryBuilder().setDaemon(true).setNameFormat("orange-grpc-executor-%d").build());
        }
        return GRPC_SERVER_EXECUTOR ;
    }

    public static ThreadPoolExecutor getNettyClientExecutor(){
        if(Objects.isNull(NETTY_CLIENT_EXECUTOR)){
            NETTY_CLIENT_EXECUTOR = new ThreadPoolExecutor(
                    EnvUtils.getAvaliableProcessor(),
                    EnvUtils.getAvaliableProcessor(), 60L, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(100),
                    new ThreadFactoryBuilder().setDaemon(true).setNameFormat("orange-grpc-executor-%d").build());
        }
        return NETTY_CLIENT_EXECUTOR ;
    }

    public static ThreadPoolExecutor getNettyServerExecutor(){
        if(Objects.isNull(NETTY_SERVER_EXECUTOR)){
            NETTY_SERVER_EXECUTOR = new ThreadPoolExecutor(
                    EnvUtils.getAvaliableProcessor(),
                    EnvUtils.getAvaliableProcessor(), 60L, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<>(100),
                    new ThreadFactoryBuilder().setDaemon(true).setNameFormat("orange-grpc-executor-%d").build());
        }
        return NETTY_SERVER_EXECUTOR ;
    }

    public static ScheduledThreadPoolExecutor getGrpcScheduleExecutor(){
        if(Objects.isNull(GRPC_SCHEDULE_EXECUTOR)){
            GRPC_SCHEDULE_EXECUTOR = new ScheduledThreadPoolExecutor(4,new NameThreadFactory("orange-grpc-schedule-"));
        }
        return GRPC_SCHEDULE_EXECUTOR ;
    }

}
