package org.orange.rpc.utils;

import java.util.ArrayList;

public class EnvUtils {

    public static int getAvaliableProcessor(){
        return Runtime.getRuntime().availableProcessors();
    }

    public static int getDefaultExecutorCors(){
        return getAvaliableProcessor() > 2 ?getAvaliableProcessor()>>1:getAvaliableProcessor();
    }

    public static int DEFAULT_EXECUTOR_QUEUE_SIZE = 100 ;

    public static final String REQUEST_BI_STREAM_SERVICE_NAME = "BiRequestStream";

    public static final String REQUEST_BI_STREAM_METHOD_NAME = "requestBiStream";

    public static final String REQUEST_SERVICE_NAME = "Request";

    public static final String REQUEST_METHOD_NAME = "request";

}
