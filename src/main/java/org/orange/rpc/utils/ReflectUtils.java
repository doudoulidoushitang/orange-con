package org.orange.rpc.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.lang.reflect.Field;


public class ReflectUtils {

    public static String toJson(Object obj){
        return JSONObject.toJSONString(obj) ;
    }

    public static <T> T toObject(String json ,Class<T> type){
        return JSONObject.parseObject(json,type);
    }

    public static Object getFieldValue(Object obj, String fieldName) {
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] serialMsg(Serializable msg) {
        byte[] data = SerializationUtils.serialize(msg);
        byte[] searialData = new byte[data.length+6];
        searialData[0] = (byte)0x68 ;
        searialData[1] = (byte)0x04 ;
        short length = (short)data.length;
        searialData[5] =(byte)(length&0xFF) ;
        searialData[4] =(byte)((length >>> 8)&0xFF) ;
        searialData[3] =(byte)((length >>> 16)&0xFF) ;
        searialData[2] =(byte)((length >>> 24)&0xFF) ;
        System.arraycopy(data, 0, searialData, 6, length);
        return searialData ;
    }

    public static Serializable deSearialMsg(byte[] data) {
        return (Serializable)SerializationUtils.deserialize(data);
    }
}
