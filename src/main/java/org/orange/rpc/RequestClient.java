package org.orange.rpc;

import org.orange.rpc.top.model.RequestRestLoad;

public interface RequestClient {

    RequestRestLoad request(RequestRestLoad request);
}
